upwardCountFeedback = duration(successful_flight_feedback_matlab.year1 - successful_flight_feedback_matlab.year1(1));
upwardCountCommand = duration(manual_dock_undock_uav23_llcp_send_message_RAW.time - successful_flight_feedback_matlab.year1(1));

figure(1)
plot(upwardCountFeedback,successful_flight_feedback_matlab.data, 'LineWidth', 2)
ylabel('Feedback State [-]')
xlabel('Time [s]')
grid on

xtickformat('mm:ss.SSS')  % Specify the tick format
xticks(upwardCountFeedback(1):seconds(5):upwardCountFeedback(end))  % Specify the desired tick locations
xtickangle(45)
ylim([0.5 5.5])
yticks(0:1:6)

xline(upwardCountCommand(1), 'r', 'LineWidth', 1)
xline(upwardCountCommand(2), 'r', 'LineWidth', 1)
xline(2.0185e-04, 'g', 'LineWidth', 1)
xline( 5.0273e-04, 'g', 'LineWidth', 1)

t_num1 = datenum(upwardCountFeedback(1724)); % 2 seconds before feedback = 3
t_num2 = datenum(upwardCountFeedback(4292));
t_numA = datenum(upwardCountCommand(1));
t_numA = t_numA - 0.000049;
t_numB = datenum(upwardCountCommand(2));
t_numB = t_numB - 0.000071;

text(t_numA, 5.3, 'Land Command')
text(t_numB, 1.75, 'Takeoff Command')
text(1.755*10^-5, 1.2, 'Airborne')
text(0.05*10^-5, 0.85, 'Standing by')
text(12.255*10^-5, 2.2, 'Landing')
text(13.955*10^-5, 1.4, 'Switch ON')
text(30.755*10^-5, 3.2, 'Docked')
text(28.7*10^-5, 2.85, 'Standing by')
text(34.755*10^-5, 4.2, 'Opening Latch')
text(41.2*10^-5, 5.2, 'Awaiting Takeoff')
text(44.0*10^-5, 2.5, 'Switch OFF')
text(53.0*10^-5, 1.2, 'Airborne')
text(51.755*10^-5, 0.85, 'Standing by')

 