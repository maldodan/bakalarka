//--------------------- llcp -------------------------------
extern "C" {
#include <llcp.h>
}

#include "dock_sys_msgs.h"

#define TX_BUFFER_LEN 255
uint8_t tx_buffer[TX_BUFFER_LEN];

LLCP_Receiver_t llcp_receiver;

long last_hb = millis();        
uint16_t num_msg_received = 0;  
//--------------------- llcp -------------------------------

//-------------- low level operation -----------------------
#include <Servo.h>

Servo myServo;
const uint8_t Switch = 2;   // Switch attached to pin2
bool SwitchState;           // 0 = drone not there, 1 = drone there
uint8_t Monitor;            // storing the Llcp read value 
uint8_t Feedback = 1;       // 1 = midair standby, 2 = landing, 3 = docked standby, 
                            // 4 = take off (opeining servo), 5 = take off (drone still there), 6 = take off (drone left)
uint8_t Error = 0;          // variable for sending errors..
                            // 40 - Switch error midair, 41 - switch error docked, 42 - switch error during latch opening
unsigned long timer = 0;    // for delaying the programme while servo is turning
//-------------- low level operation -----------------------

void setup()
{
//--------------------- llcp -------------------------------
  llcp_initialize(&llcp_receiver);    // inicialize the llcp receiver struct  
//--------------------- llcp -------------------------------

//-------------- low level operation ----------------------
  Serial.begin(115200);
  pinMode(Switch, INPUT_PULLUP);      // built in on PCB!!
  myServo.attach(3);                  // servo attached to pin 3 
  myServo.write(30);                  // initial position .. open
//-------------- low level operation ----------------------
}

void loop() 
{
//--------------------- llcp -------------------------------
if (millis() - last_hb >= 1000) {     
    last_hb = millis();
    send_heartbeat();                 // sending heartbeat
  }

send_data();                          // sending feedback
delay(10); 
//--------------------- llcp -------------------------------

//-------------- low level operation ----------------------
SwitchState =! digitalRead(Switch);   // reading status of the switch
  
// Docking logic  ---------------

// 1 = Midair - accepts ROS commands
  if(SwitchState == 0 && Feedback == 1) 
  { 
    Error = 0;                        // not an error state
    receive_message();                // input from LLCP
    if(Monitor==2)                    // only accepts land command
      {
        Feedback = Monitor;           
      }
  }

// 2 = Landing - does not accept ROS commands
  if(SwitchState == 0  && Feedback == 2) 
  {  
    Error = 0;                         // not an error state
    timer = millis();                  // reset the timer every time the switch is untriggered
  }

// 3 = Landing (Switch Triggered) - does not accept ROS commands (internal state only - is not transmitted)
  if(SwitchState == 1 && Feedback == 2) 
  {
    Error = 0;                          // not an error state
    if (millis() - timer >= 2000) {     // switch needs to be closed of 2s
      myServo.write(75);                // closed position  
      Feedback = 3;               
    }
  }  

// 4 = docked accepts ROS commands
  if(SwitchState == 1 && Feedback == 3)  
  { 
    Error = 0;                          // not an error state
    timer = millis();                   // timer reset 
    receive_message();                  // input from LLCP
    if(Monitor==4)                      // can only accept takeoff command                  
    {
      Feedback = Monitor;      
    }  
  }    

//5 = taking off (Turning latch) - does not accept ROS commands 
  if(SwitchState == 1 && Feedback == 4)  
  { 
    Error = 0;                        // not an error state
    myServo.write(40);                // open position
    if (millis() - timer >= 2000) {   // delay 2s before changing state
      Feedback = 5;                   
    }
  }

//6 = taking off (Awaiting takeoff) - does not accept ROS commands
  if(SwitchState == 1 && Feedback == 5)  
  { 
    Error = 0;                        // is not an error state
  }

//6 = taking off (Switch untriggered) - does not accept ROS commands (internal state only - is not transmitted)
  if(SwitchState == 0 && Feedback == 5)  
  { 
   Error = 0;                         // is not an error state
   Feedback = 1;                      // Docked stanby
  }

// Docking logic  ---------------

// Errors ----------------------

// Switch is triggered when the drone is not expected to be docked 
  if(SwitchState == 1 && Feedback == 1)   
  {
   Error = 40;                         // is an error state
  }

// switch is untriggered when the drone is docked  
  if(SwitchState == 0 && Feedback == 3)   
  {
    Error = 41;
  }
  
// sent take off request but the switch is untriggered before servo can turn
// The takeoff command was sent but the switch is untriggered before the timer runs out     
  if(SwitchState == 0 && Feedback == 4) 
  { 
    timer = millis();    // timer reset
    Error = 42;
  }
// Errors ----------------------

//-------------- low level operation ----------------------
} 


// ------------------- Llcp functions --------------------------

// send heartbeat function ---------------------

void send_heartbeat() 
{ 
  heartbeat_msg my_msg;
  uint16_t msg_len;

  my_msg.id = HEARTBEAT_MSG_ID;
  my_msg.is_running = true;

  //llcp_prepareMessage will fill your TX buffer while returning the number of bytes written
  msg_len = llcp_prepareMessage((uint8_t*)&my_msg, sizeof(my_msg), tx_buffer);

  //send the message out over the serial line
  for (int i = 0; i < msg_len; i++) 
    {
      Serial.write(tx_buffer[i]);
    }
}

// send heartbeat function ---------------------

// send data function ---------------------

void send_data() 
{
  dock_sys_feedback_msg my_msg;
  uint16_t msg_len;

// fill the message with data
  my_msg.id = DOCK_SYS_FEEDBACK_ID;
  if(Error >= 40 && Error <= 42)
    {
      my_msg.feedback = Error;
    }
  else if(Error == 0)
    {
      my_msg.feedback = Feedback;
    }
  
//llcp_prepareMessage will fill your TX buffer while returning the number of bytes written
  msg_len = llcp_prepareMessage((uint8_t*)&my_msg, sizeof(my_msg), tx_buffer);

//send the message out over the serial line
  for (int i = 0; i < msg_len; i++)
    {
      Serial.write(tx_buffer[i]);
    }
}

// send data function ---------------------

// receive message function ------------

bool receive_message() 
{
  uint16_t msg_len;
  bool got_valid_msg = false;
  LLCP_Message_t* llcp_message_ptr;

  while (Serial.available() > 0) 
  {
      bool checksum_matched;
      uint8_t char_in = Serial.read();

//individual chars are processed one by one by llcp, if a complete message is received, llcp_processChar() returns true
      if (llcp_processChar(char_in, &llcp_receiver, &llcp_message_ptr, &checksum_matched)) 
      {
        if (checksum_matched) 
        {
          num_msg_received++;
          switch (llcp_message_ptr->payload[0]) 
          {
            case DOCK_SYS_COMMAND_ID: 
            {
              dock_sys_command_msg *received_msg = (dock_sys_command_msg *)llcp_message_ptr;

              Monitor = received_msg->command;

              got_valid_msg = true;
              break;
            }
            case HEARTBEAT_MSG_ID: 
            {
              got_valid_msg = true;
              break;
            }
        }
      return true;
      }
    }
  }
  return got_valid_msg;    
}  

// receive message function ------------

// ------------------- Llcp functions --------------------------
