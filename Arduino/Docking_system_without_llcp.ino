#include <Servo.h>

Servo myServo;
const int Switch = 2;   // pin2

bool SwitchState; // 0 = drone not there, 1 = drone there
int Monitor; // storing the Serial Monitor value 
int Feedback = 1; // 1 = midair standby, 2 = land, 3 = docked standby, 
                            //4 = take off (opeining servo)

unsigned long timer = 0;  // for delaying the programme while servo is turning

void setup()
{
  Serial.begin(9600);
  pinMode(Switch, INPUT_PULLUP);  // change when you have the PCB
  myServo.attach(3);
}

void loop() 
{

delay(100);    // Serial monitor slow down

 // input from the switch
  SwitchState =! digitalRead(Switch);   // INPUT_PULLUP
  
// debugging prints  ------------------
/*
  Serial.print("Switch is in the state: ");
  Serial.println(SwitchState);
*/
/*
  Serial.print("Monitor:");
  Serial.println(Monitor);
*/
/*
  Serial.print("Timer:");
  Serial.println(timer);
*/
//  -----------------------


// Docking logic  ---------------

// 1 = midair.. accepts ROS commands
  if(SwitchState == 0 && Feedback == 1) 
  { 
    Serial.println("Feedback: Drone is midair");

    // input from ROS
    if (Serial.available() > 0) 
      {    
        Monitor = Serial.parseInt();  // value is x0 and I have to ignore the 0   
      }
    if(Monitor==2)                    // can only accept land command
      {
      Feedback = Monitor;           //Feedback holds the value of command      
      }  
  }

// 2 = landing.. doesnt accept ROS commands
  if(SwitchState == 0  && Feedback == 2) 
  {  
    Serial.println("Feedback: Drone is landing");
    timer = millis();   // reset the timer every time the switch is untriggered
  }

// landing switch triggered.. doesnt accept commands
  if(SwitchState == 1 && Feedback == 2) 
  {
    Serial.println("Feedback: Drone inside");
    
      if (millis() - timer >= 2000) {   //delay 500ms
      myServo.write(90);                    //  closed position  
      Feedback = 3;                         // Docked stanby
    }
    
  }  

// 3 = docked accepts ROS commands
  if(SwitchState == 1 && Feedback == 3)  
  { 
    timer = millis();   // reset the timer before undocking 
    Serial.println("Feedback: Drone is docked");    
    
    // input from ROS
    if (Serial.available() > 0) 
      {    
        Monitor = Serial.parseInt();
      }
    if(Monitor==4)              // can only accept take off command                  
      {
      Feedback = Monitor;      
      }  
  }    

//4 = taking off (opening servo).. doesnt accept ROS commands.. 
  if(SwitchState == 1 && Feedback == 4)  
  { 
    Serial.println("Feedback: Drone is taking off");
    myServo.write(135);               // open position
    
    if (millis() - timer >= 2000) {   //delay 500ms
      Feedback = 5;                   // waiting for drone takeoff
    }
  }

//5 = taking off.. Waiting for drone to take off 
  if(SwitchState == 1 && Feedback == 5)  
  { 
    Serial.println("Feedback: Waiting for drone takeoff");
  }

//5 = taking off.. drone took off
  if(SwitchState == 0 && Feedback == 5)  
  { 
    Serial.println("Feedback: Drone has left");
    Feedback = 1;                   // Docked stanby
  }
  
// Docking logic  ---------------

// Errors ----------------------
  if(SwitchState == 1 && Feedback == 1)   // switch is triggered when the drone is not expected to be docked 
  {
    Serial.println("Feedback: Switch error");
  }

  if(SwitchState == 0 && Feedback == 3)   // switch is untriggered but drone is docked
  {
    Serial.println("Feedback: Switch error");
  }  

  if(SwitchState == 0 && Feedback == 4) // sent take off request but the switch is untriggered before servo can turn
  { 
    timer = millis();    // reset the timer for servo delay
    Serial.println("Feedback: Takeoff happened too fast or switch error");
  }
// Errors ----------------------

}
