#define DOCK_SYS_COMMAND_ID 50
#define DOCK_SYS_FEEDBACK_ID 51
#define HEARTBEAT_MSG_ID 52

struct __attribute__((__packed__)) dock_sys_command_msg
{
  uint8_t  id;  //has to have an ID
  uint8_t  command;  
  /*    2 = land
        4 = take off
  */
};

struct __attribute__((__packed__)) dock_sys_feedback_msg
{
  uint8_t  id;  //has to have an ID
  uint8_t  feedback;  
  /*    1 = midair
        2 = landing
        3 = docked
        4 = taking off
  */
};

struct __attribute__((__packed__)) heartbeat_msg
{
  uint8_t  id;
  bool     is_running;
};
