#include <ros/package.h>
#include <ros/ros.h>
#include <mrs_msgs/Llcp.h>
#include <std_msgs/UInt8.h> //standart ros noetic message for commands
#include <string>

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include "../firmware/dock_sys_msgs.h"    // for publishing feedback to topics
#include "mrs_dock_sys/DockSysCommand.h"             // for getting service input... 
// service in: 2 = land, 4 = take off

namespace dock_sys_llcp
{

/** class DockSysLlcp **/

class DockSysLlcp : public nodelet::Nodelet {

public:
  virtual void onInit();

private:
  ros::Timer send_timer_;
  // Declaring our callbacks
  void callbackSendTimer(const ros::TimerEvent &event);
  void callbackReceiveMessage(const mrs_msgs::LlcpConstPtr &msg);             
  bool callbackReceiveDockSysCommand(mrs_dock_sys::DockSysCommand::Request &req, mrs_dock_sys::DockSysCommand::Response &res);

  ros::NodeHandle nh_;

  // Preparing llcp subscriber and publisher
  ros::Subscriber llcp_subscriber_;
  ros::Publisher  llcp_publisher_;  
  
  // Publisher for feedback
  ros::Publisher feedback_publisher_;

  // Service for DockSys control
  ros::ServiceServer dock_sys_state_service;

  // uav name passed in launchfile
  std::string uav_name_;
  
  ros::Time interval_      = ros::Time::now();
  ros::Time last_received_ = ros::Time::now();

  uint8_t Command = 0;    // global variable for storing incomming command from service

  bool is_initialized_ = false;

};

void DockSysLlcp::onInit() {

  // Get node paramters
  nh_ = ros::NodeHandle("~");

  ros::Time::waitForValid();

   nh_.param("uav_name", uav_name_, std::string("uav")); 
  /* nh_.param("portname", portname_, std::string("/dev/ttyUSB0")); */
  /* nh_.param("baudrate", baudrate_, 115200); */
  /* nh_.param("publish_bad_checksum", publish_bad_checksum, false); */
  /* nh_.param("use_timeout", use_timeout, true); */
  /* nh_.param("serial_rate", serial_rate_, 5000); */
  /* nh_.param("serial_buffer_size", serial_buffer_size_, 1024); */


  // publish the received feedback on the topic "feedback" using a uint8 msg
  feedback_publisher_    = nh_.advertise<std_msgs::UInt8>("feedback", 1);
   
  // Initialize LLCP publisher and subscriber
  llcp_publisher_ = nh_.advertise<mrs_msgs::Llcp>("llcp_out", 1);
  llcp_subscriber_ = nh_.subscribe("llcp_in", 10, &DockSysLlcp::callbackReceiveMessage, this, ros::TransportHints().tcpNoDelay());
  
  // subscribe to the "dock_sys_command" topic to receive commands from the terminal
  dock_sys_state_service  = nh_.advertiseService("terminal_command", &DockSysLlcp::callbackReceiveDockSysCommand, this);

  is_initialized_ = true;

}


// | ------------------------ callbacks ------------------------ |

// GETING COMMAND FROM ROS SERVICE AND SENDING IT VIA LLCP TO MCU
bool DockSysLlcp::callbackReceiveDockSysCommand([[maybe_unused]] mrs_dock_sys::DockSysCommand::Request &req, mrs_dock_sys::DockSysCommand::Response &res) {
 
  if (!is_initialized_) {
    return false;
  }

// storing the value of incomming command 
  Command = req.COMMAND;    
// setting the success response message to true
  res.success = true;

  dock_sys_command_msg msg_out;   
  mrs_msgs::Llcp llcp_msg;    
  
  /* message structure.. look into "dock_sys_msgs.h" for more info*/
  msg_out.id = DOCK_SYS_COMMAND_ID;
  msg_out.command = Command; // passing the received command to msg_out

/* LLCP transmission.. llcp protocol specific.. llcp needs an array to work */
  uint8_t *msg_ptr = (uint8_t *)&msg_out;

//  making msg_out into a vector called llcp_msg
  for (int i = 0; i < sizeof(msg_out); i++) {
    llcp_msg.payload.push_back(msg_ptr[i]);
  }

// publishing the llcp_msg for the MCU to subscribe
  llcp_publisher_.publish(llcp_msg);  


// ROS terminal prints
  ROS_INFO_STREAM("Sending data message -> id: " << int(msg_out.id));
  ROS_INFO_STREAM("Sending data message -> command: " << unsigned(msg_out.command));

  return true;
}


//RECEIVING MESSAGE VIA LLCP
void DockSysLlcp::callbackReceiveMessage(const mrs_msgs::LlcpConstPtr &msg) {

  if (!is_initialized_) {
    return;
  }

  // llcp is working with arrays, so we need to convert the payload from the ROS message into an array
  uint8_t payload_size = msg->payload.size();
  uint8_t payload_array[payload_size];
  std::copy(msg->payload.begin(), msg->payload.end(), payload_array);

  /* sorting the messages based on the message ID */
  switch (payload_array[0]) {

  // IF THE INCOMMING MESSAGE IS FEEDBACK FROM MCU
    case DOCK_SYS_FEEDBACK_ID: 
    {

      // THREADING INCOMMING PAYLOAD INTO "FEEDBACK" STRUCTURE DEFINED IN "dock_sys_msgs.h"
      dock_sys_feedback_msg *received_msg = (dock_sys_feedback_msg *)payload_array;

      // SAVING FEEDBACK INTO A LOCAL VARIABLE
      uint8_t state = received_msg->feedback;
      
      /* message structure */
      std_msgs::UInt8 feedback_msg;  // opt/noetic/ros/std_msgs/UInt8.h
      feedback_msg.data = state;     // MAKING STRUCT FOR ROS TRANSMISSION 
      
	    //PUBLISHING ON THE "feedback" TOPIC
      try {
        feedback_publisher_.publish(feedback_msg);
      }
      //  EXCEPTION
      catch (...) {
        ROS_ERROR("Exception caught during publishing topic %s", feedback_publisher_.getTopic().c_str());
      }

      // FEEDBACK CLASIFICATION
      switch (received_msg->feedback)
      {
        case 1: 
        {
          ROS_INFO_STREAM("Airborne - standing by");
          break;
        } 
        case 2: 
        {
          ROS_INFO_STREAM("Landing");
          break;
        }
        case 3: 
        {
          ROS_INFO_STREAM("Docked - standing by");
          break;
        }
        case 4:
        {
          ROS_INFO_STREAM("Takeoff - Opening latch");
          break;
        }
        case 5: 
        {
          ROS_INFO_STREAM("Takeoff - Awaiting takeoff");
          break;
        }
        case 40: 
        {
          ROS_INFO_STREAM("Sensor Error - Airborne");
          break;
        }
        case 41: 
        {
          ROS_INFO_STREAM("Sensor Error - Docked");
          break;
        }
        case 42: 
        {
          ROS_INFO_STREAM("Sensor Error - takeoff");
          break;
        }
        default:
        {
          ROS_INFO_STREAM("Unknown Feedback comming in");
          break;
        }
      }

    break;
    }

    case HEARTBEAT_MSG_ID: 
    {

      // THREAD INCOMMING PAYLOAD INTO FEEDBACK DEFINED IN "dock_sys_msgs.h"
      heartbeat_msg *received_msg = (heartbeat_msg *)payload_array;

      ROS_INFO_STREAM("!Received heartbeat!");
      break;
    }
    
    // IF MESSAGE ID RECEIVED IS DIFFERENT FROM 50, 51 OR 52
    default: 
    {
      ROS_ERROR_STREAM("Received unknown message with id " << int(payload_array[0]));
      break;
    }
  }
}


/** | ------------------------ routines ------------------------ | **/


}  //  idk.. I suppose that ends the script or smth

PLUGINLIB_EXPORT_CLASS(dock_sys_llcp::DockSysLlcp, nodelet::Nodelet);
